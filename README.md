# Donutty Timer - countdown timer script

Beautiful javascript countdown timer and clock, using svg donuts for visualization.

![Clock and countdown timer script](doc/img/screenshot.png)

See [live examples](https://codepen.io/i1group/pen/vqLvZO).

## Dependencies

Donutty Timer uses [donutty](https://github.com/simeydotme/donutty/) script and requires it for clock face.

## Including script files

Include the `./dist/` file:
```html
<script src="/dist/donutty-timer.all.min.js"></script>
```
Or in case if you already use donutty library:
```html
<script src="/dist/donutty.min.js"></script>
<script src="/dist/donutty-timer.min.js"></script>
```

## How to use javascript countdown timer

There's a couple of ways to configure **DonuttyTimer** depending on how you prefer:
#### 1. html data attributes
Using `data-` attributes in the html to configure timer options

```html
<!-- this will create a 1 minute countdown timer with current value 45s -->

<div id="timer" data-donutty-timer data-max="60" data-value="45"></div>
```

#### 2. js initialisation
Using javascript to initialise and configure **donutty timer**'s options

```js
// this will create a 1 minute countdown timer with current value 45s

var timer = new DonuttyTimer('#timer', {max: 60, value: 45});
```

## Documentation

+ See [documentation page](doc/README.md) for options and methods.
+ Have a look at the [examples](https://codepen.io/i1group/pen/vqLvZO).

## Bugs & contributing

+ Please ask general questions through [Stack Overflow](http://stackoverflow.com/questions/tagged/donuttytimer)
+ Feel free to raise bugs/issues if found, and submit pull-requests. Even the tiniest contributions to the script or to the documentation are very welcome.
