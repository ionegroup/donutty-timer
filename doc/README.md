# Donutty Timer Documentation

Contents:

[TOC]

## Create timer

Include the `./dist/` file:
```html
<script src="/dist/donutty-timer.all.min.js"></script>
```
Or in case if you already use donutty library:
```html
<script src="/dist/donutty.min.js"></script>
<script src="/dist/donutty-timer.min.js"></script>
```

There's a couple of ways to configure [**DonuttyTimer**](../../master) depending on what you prefer:

**1. html data attributes**

Use `data-` attributes in the html to configure timer options

```html
<!-- this will create a 1 minute timer with current value 45s -->

<div id="timer" data-donutty-timer data-max="60" data-value="45"></div>
```

**2. js initialisation**

Use javascript to initialise and configure **donutty timer**'s options

```js
// this will create a 1 minute timer with current value 45s

var timer = new DonuttyTimer('#timer', {max: 60, value: 45});
// or
var timer = new DonuttyTimer(document.querySelector('#timer'), {max: 60, value: 45});
// or
var timer = new DonuttyTimer(jQuery('#timer').get(0), {max: 60, value: 45});
```

## Accessing timer from outside

If you have no `timer` variable, you can always get it like this:
```js
// Get timer container (tag) and timer itself
var $timer = document.querySelector('.magic-timer'),
timer = $timer ? $timer.donuttyTimer : null;
```

And then play with timer as you wish:
```js
if (timer) {
  // Get timer container classes
  console.log(timer.$wrapper.classList);
  // Get current timer state (will print {min, max, value})
  console.log(timer.getState());
  // Change time format
  timer.set('format', {hour: true, min: true, sec: true});
  // Pause / play timer
  timer.pause();
  timer.play();
}
```

## Container classes

Following classes will be added to timer's container to represent it's state:

+ dt-status-init
+ dt-status-play
+ dt-status-pause
+ dt-status-stop
+ dt-runout
+ dt-end

## Options

| Option | Type | Default | Description |         |
| -----: | :--- | :------ | :---------- | :------ |
| **max** | `Integer` | `60` | The maximum value of the timer (seconds). | [Example2](https://codepen.io/i1group/pen/JQKXyd) |
| **min** | `Integer` | `0` | The minimum value of the timer (seconds). | [Example2](https://codepen.io/i1group/pen/JQKXyd) |
| **value** | `Integer` | `min`/`max` | Current time value for donut (seconds). By default: `min` (i.e. `0`) for clocks (if `delta > 0`) and `max` (i.e. `60`) for timers. | [Example2](https://codepen.io/i1group/pen/JQKXyd) |
| **valuedisplay** | `Integer` | `min`/`max` | Current time value for digits / text (seconds). [@see*1](#markdown-header-1-transition-transitionsync-and-callbacksafterstop) | |
| **delta** | `Integer` | `-1` | Number of seconds to change value for on every tick. Set `-1` for countdown timer, `1` for clocks. | [Example5](https://codepen.io/i1group/pen/XLKjzq) |
| **round** | `Boolean` | `true` | If the edges of the donut (clock face) are rounded or not. | [Example1](https://codepen.io/i1group/pen/vqLvZO) |
| **circle** | `Boolean` | `true` | Does the donut (clock face) create a complete circle or not. | [Example1](https://codepen.io/i1group/pen/vqLvZO) |
| **radius** | `Integer` | `50` | The radius of the donut (width and height, essentially, but can be made auto with css). | [Example1](https://codepen.io/i1group/pen/vqLvZO) |
| **thickness** | `Integer` | `10` | How thick the actual donut track is. | [Example1](https://codepen.io/i1group/pen/vqLvZO) |
| **padding** | `Integer` | `4` | Padding between the background (track) and the donut. | [Example1](https://codepen.io/i1group/pen/vqLvZO) |
| **bg** | `String` | `rgba(70, 130, 180, .15)` | The color of the background (track). | [Example1](https://codepen.io/i1group/pen/vqLvZO) |
| **color** | `String` | `mediumslateblue` | Color of the actual donut. | [Example1](https://codepen.io/i1group/pen/vqLvZO) |
| **transition** | `String` | `all 200ms linear` | The animation which runs on the donut (clock face). | [Example1](https://codepen.io/i1group/pen/vqLvZO) |
| **transitionsync** | `Boolean` | `true` | Will the donut animation be synced with the time. [@see*1](#markdown-header-1-transition-transitionsync-and-callbacksafterstop) | [Example3](https://codepen.io/i1group/pen/qzNNqj) |
| **callbacksafterstop** | `Boolean` | `false` | Should callbacks be called after stop. [@see*1](#markdown-header-1-transition-transitionsync-and-callbacksafterstop) | |
| **autostart** | `Integer` | `0` | Delay (ms) to start timer. `0` - start immediately, `-1` - don't start timer automatically. | [Example1](https://codepen.io/i1group/pen/vqLvZO) |
| **valueonstop** | `String` | `reset` | What value should be set when timer stops (i.e. ahead of time on Stop button click). `reset` - behave like the time had reached it's end, `initial` - set initial value. | [Example3](https://codepen.io/i1group/pen/qzNNqj) |
| **stoponend** | `Boolean` | `true` | Will timer stop when time is up. Set `false` to continue ticking (but keep in mind that it would work good with `circle = true`). | [Example3](https://codepen.io/i1group/pen/qzNNqj) |
| **runout** | `Integer` | `0` | Time value (sec) that indicates that time is running out. | [Example5](https://codepen.io/i1group/pen/XLKjzq) |
| **speed** | `String` | `1000` | Timer speed (ms). Normal ticking speed is 1s (1000ms), but you can create crazy timers by changing `speed` and `delta` options. | [Example5](https://codepen.io/i1group/pen/XLKjzq) |
| **format** | `Object` | `{min: true, sec: true}` | Time format (indicates what should be displayed). Full format is: `{day: true, hour: true, min: true, sec: true}`. [@see*2](#markdown-header-2-format) | [Example2](https://codepen.io/i1group/pen/JQKXyd) |
| **separator** | `String` | `:` | Timer digits separator. | [Example2](https://codepen.io/i1group/pen/JQKXyd) |
| **tag** | `String` | `span` | Timer digits wrapper tag. | [Example2](https://codepen.io/i1group/pen/JQKXyd) |
| **labels** | `Object` | `null` | Text that would be displayed instead of digits on different states. [@see*3](#markdown-header-3-labels) | [Example4](https://codepen.io/i1group/pen/VJjKYq) |
| **buttons** | `Object` | `null` | Elements (selectors or DOM objects) wich would be used as timer buttons. [@see*4](#markdown-header-4-buttons) | [Example4](https://codepen.io/i1group/pen/VJjKYq) |
| **callbacks** | `Object` | `null` | Callbacks for different timer events. [@see*5](#markdown-header-5-callbacks) | [Example5](https://codepen.io/i1group/pen/XLKjzq) |
| **gettext** | `Function` | `null` | A custom function returning text/html of timer digits (text). [@see*6](#markdown-header-6-gettext) | [Example5](https://codepen.io/i1group/pen/XLKjzq) |
| **responsive** | `Boolean` | `true` | By default donut (clock face) is steatched to the width of parent container. Setting this value to `false` makes the clock size fixed (calculated using the `radius` property). If you experience wrong middle text alignment with default behavior, then set the same value to both `width` and `height` of the parent container. | [Example2](https://codepen.io/i1group/pen/JQKXyd) |


#### *1 transition, transitionsync and callbacksafterstop
As you may know after changing property of some element (e.g. opacity, color, width) it would not be changed immediatedly when `transition` property is set.
It would be smoothly modified with animation and will be completed after some time (transition-duration).
This behavior causes the donut visual value always be behind the actual clock time (by transition-duration).

That's where `transitionsync` property can be useful - when set to `true` (default) it tries to change donut value before the clock value so the donut visual value will match real clock value.
But keep in mind that setting long transitions (longer than `speed` which is 1s by default) can cause the donut animation look not as expected.

Also keep in mind that there are 2 properties representing current time: `value` (for donut) and `valuedisplay` (for digits / text displayed on the timer). If you need to get current time - `valuedisplay` is right for that, but if you want to set new value - at least update `value`, though it may be a good idea to update both properties:
```js
this.setState({value: newValue, valuedisplay: newValuedisplay});
```
However if `transitionsync` is `false`, you may use `value` property only to get and set as it will always match `valuedisplay`.

Another property that is related to `transitionsync` is `callbacksafterstop`. Since `value` and `valuedisplay` properties are updated separately, there is a probability that user will click Stop button (if you have one of course) after `value` is updated, but before `valuedisplay` is updated.

By default `valuedisplay` would not be updated and stop method would be called in this case. Also as `tick`, `runout` and `end` callbacks are linked to `valuedisplay` property, these callbacks would not be called by default.

So if need to call these callbacks due to some reasons any way you can set `callbacksafterstop` to `true`. But keep in mind that all these callbacks would most probably be called after the stop method.


#### *2 format
To make it work as expected don't skip format attributes in the middle. For example:
```js
var options = {
  max: 300,
  // bad:
  format: {day: true, min: true},
  format: {hour: true, sec: true},
  // good:
  format: {day: true, hour: true, min: true},
  format: {hour: true, min: true, sec: true},
}
```

Format also can be set via `data-` attribute. In this case it should be valid stringified JSON, but instead of double quotes `"` single quotes `'` should be used:
```html
<div
  id="magic-timer"
  data-donutty-timer
  data-format="{'hour': true, 'min': true, 'sec': true}"
></div>
```


#### *3 labels
All labels are optional. Following are available:
```js
var options = {
  max: 300,
  labels: {
    init: 'press Play to start the timer',  // after init (before start)
    pause: "I'm waiting",                   // during pause
    stop: 'Time is up',                     // after stop
  },
}
```

Labels also can be set via `data-` attribute. In this case it should be valid stringified JSON, but instead of double quotes `"` single quotes `'` should be used:
```html
<div
  id="magic-timer"
  data-donutty-timer
  data-autostart="-1"
  data-buttons="{'playpause': '#magic-timer'}"
  data-labels="{'init': 'Click me to play / pause', 'pause': 'I&#39;m waiting', 'stop': 'Time is up'}"
></div>
```
Note that if you need to use single quote `'` in the text - you should use something like `&#39;` instead.
Or initialize timer with js to avoid this kind of limitations.


#### *4 buttons
All buttons are optional. Following are available:
```js
var options = {
  max: 300,
  buttons: {
    play: '.toolbar .play',                          // start / play timer
    pause: '.toolbar .pause',                        // pause timer
    stop: document.querySelector('.toolbar .stop'),  // stop timer
    playpayse: jQuery('#play-pause-toggle').get(0),  // play / pause toggle
  },
}
```
You can pass css selector or DOM object for buttons.
Also buttons can be set via `data-` attribute (using css selectors). In this case it should be valid stringified JSON, but instead of double quotes `"` single quotes `'` should be used:
```html
<div
  id="magic-timer"
  data-donutty-timer
  data-buttons="{'play': '.toolbar .play', 'pause': '.toolbar .pause', 'stop': '.toolbar .stop', 'playpause': '#play-pause-toggle'}"
></div>
```
All buttons will get `dt-status-` [classes](#markdown-header-container-classes) reflecting timer state.


#### *5 callbacks
All callbacks are optional. Following are available:
```js
var options = {
  max: 300,
  callbacks: {
    init: function(){...},    // initialized and ready
    start: function(){...},   // is started for the first time (executed before `play`)
    play: function(){...},    // is playing (after start or after pause)
    pause: function(){...},   // is paused
    tick: function(){...},    // executed on every clock tick
    runout: function(){...},  // time is running out (set `runout` option to make it work)
    end: function(){...},     // time is up (but timer may still be ticking if `stoponend = false`)
    stop: function(){...},    // is stopped (time is up or user clicked Stop button)
  },
}
```

In all callbacks `this` would point to `donuttyTimer` itself, and you can use it as you wish. For example:
```js
var options = {
  max: 300,
  runout: 30,
  callbacks: {
    runout: function(){
      var state = this.getState();
      console.log('Hurry! ' + (state.max - state.value) + ' seconds already passed, just ' + state.value + ' left'.);
    },
  },
};
```


#### *6 gettext

You may pass a custom function to the `gettext` option which returns timer digits (or other text) that will be displayed on clock face.
If you want you may reuse built-in [getText()](#markdown-header-gettextstate) or [getTime()](#markdown-header-gettimevalue) methods inside your custom `text()`.

**Parameters**

`{Object} state`: Current timer state like: `{min: 0, max: 300, value: 200, valuedisplay: 200, bg: 'aquamarine', color: 'slategrey'}`.

**Result**

`{String}` Text/html that would be displayed on clock face.

**Example**

```js
var options = {
  max: 300,
  text: function(state) {
    // return percentage of spent time
    return Math.round(state.valuedisplay / (state.max - state.min) * 100) + '%';
  },
}
```



## Methods

There are some methods available for updating/changing values on the timer/clock
after it has been created. These are accessible by creating a reference
to the timer in javascript.

```js
// Create the timer first
var timer = new Donutty('#timer', {max: 300, value: 100});

// Then lets modify some values
timer.set('min', 100).set('max', 300).set('bg', 'aquamarine').set('color', 'slategrey');
// or
timer.setState({min: 100, max: 300, bg: 'aquamarine', color: 'slategrey'});
```

All methods that do not return some result are **chainable** (returning `this` object).


### ➙ getOptionsFromTag()

Gets options from `data-` attributes of a timer container tag.

**Result**

`{Object}` Options to initialize donutty timer.



### ➙ getTransitionDuration()

Returns transition duration.
Parses transition option like 'all 200ms linear' or 'all 1s' to get duration from it.

**Result**

`{Integer}` Transition duration in ms.
If options.transitionsync is false will always return 0.



### ➙ init()

Create donutty timer.
Normally you shouldn't call this function, it will be called automatically.



### ➙ assignButtons()

Assign onclick events for buttons (from options.buttons property).
Normally you shouldn't call this function, it will be called automatically.



### ➙ reset(doit)

Reset timer state. Is called automatically on `stop`.

**Parameters**

`{String} doit`: What to do with time value on reset. Possible values:

+ reset - reset value to min for countdowns, to max for clocks.
+ initial - set initial value.



### ➙ callback(name)

Execute a callback function. See [callbacks](#markdown-header-5-callbacks) option for list of available callbacks.

**Parameters**

`{String} name`: Callback name.



### ➙ initStyles()

Initialize timer styles to align time (text) to the middle of container.
Is called automatically on `init()`, normally shouldn't be used in custom functions.



### ➙ updateSize()

Resize timer container - make it's size match donut (svg) size.
Is called automatically on `init()`, normally shouldn't be used in custom functions.



### ➙ updateText()

Update timer (clock face) text.



### ➙ getText(state)

Return timer text (usually current clock/timer time).

Uses `labels` option to display text according to current status and `getTime()`
method to display current time. Is called on every clock tick (time value change).

**Parameters**

`{Object} state`: Current timer state.

**Result**

`{String}` Html code of timer text.



### ➙ getTime(value)

Return current time fractions.

**Parameters**

`{Integer} value`: Current time (sec).

**Result**

`{Object}` Current time as object, that was build according to the timer options.
Default format looks like: `{min: '04', sec: '59'}`.



### ➙ getState()

Get current state (most useful options).

**Result**

`{Object}` Current donutty state values like:
`{min: 0, max: 60, value: 50, valuedisplay: 50, bg: 'blue', color: 'red'}`.



### ➙ setState(newState)

Set new values for multiple properties.

**Parameters**

`{Object} newState`: New values in {key: value} format like:
`{min: 0, max: 300, value: 200, valuedisplay: 200}`.



### ➙ get(prop)

Get current state property.

**Parameters**

`{String} prop`: Property name.

**Result**

`{Mixed}` Current value for a timer property.



### ➙ set(prop, val)

Set new value for property.
Note: if options.transitionsync is false then new value for `value` property will also be set for `valuedisplay` property.

**Parameters**

`{String} prop`: Property name.

`{Mixed} val`: New property value.



### ➙ adjustValue(delta)

Add/substract some time from current value.

**Parameters**

`{Integer} delta`: Number of seconds to add (can be negative).



### ➙ is(status)

Check if current timer status is the same as specified.

**Parameters**

`{String} status`: Status name. Possible values:

+ started
+ play (or playing)
+ pause (or paused)
+ stop (or stopped)

**Result**

`{Boolean}` True - if current timer status matches `status` param, false otherwise.



### ➙ getStatus()

Get current timer status.

**Result**

`{String}` Current status.



### ➙ setStatus(status)

Set timer status.
Normally shouldn't be used in custom functions.

**Parameters**

`{String} status`: New status name.



### ➙ setStatusClass(status)

Set class representing status to timer container and buttons.
Normally shouldn't be used in custom functions.

**Parameters**

`{String} status`: Status name.



### ➙ start()

Start the timer (reset some values and start playing).



### ➙ tick()

Make the timer ticking (change current time).
Normally shouldn't be used in custom functions.



### ➙ pause()

Pause the timer.



### ➙ play()

Start the timer or continue ticking (after pause).



### ➙ stop()

Stop the timer (reset and finish any activity).
