var gulp = require( "gulp" ),
    concat = require( "gulp-concat" ),
    eslint = require( "gulp-eslint" ),
    header = require( "gulp-header" ),
    rename = require( "gulp-rename" ),
    uglify = require( "gulp-uglify" ),
    pkg = require( "./package.json" ),
    uglifyOpts = { output: { comments: /(^!|@preserve)/i } };

gulp.task( "minify", [ ], function() {
    return gulp.src([ "./dist/donutty-timer.js" ])
        .pipe( uglify( uglifyOpts ) )
        .pipe( rename( "donutty-timer.min.js" ) )
        .pipe( gulp.dest( "./dist" ) );

});

gulp.task( "concat", [ ], function() {
    return gulp.src([ "./node_modules/donutty/dist/donutty.min.js", "./dist/donutty-timer.min.js" ])
        .pipe( concat( "donutty-timer.all.min.js" ) )
        .pipe( gulp.dest( "./dist" ) );
});

gulp.task( "default", [ "minify", "concat" ]);
